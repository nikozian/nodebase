var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/users').model;

module.exports = function(passport) {
  passport.use(User.createStrategy());
  passport.serializeUser(User.serializeUser());
  passport.deserializeUser(User.deserializeUser());
}
