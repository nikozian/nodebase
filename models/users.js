var mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');

var schema = new mongoose.Schema({
});

schema.plugin(passportLocalMongoose, { usernameField: 'email', usernameUnique: true });
var model = mongoose.model('users', schema);
module.exports = {
  schema: schema,
  model: model
}
